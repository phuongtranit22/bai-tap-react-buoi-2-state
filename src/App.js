
import './App.css';
import Glass from "./components/Glass";

function App() {
  return (
    <div className="App">
      <Glass></Glass>
    </div>
  );
}

export default App;
