import './Glass.css'

import glassesJson from './dataGlasses.json'
import {useState} from "react";

function Glass() {
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [des, setDes] = useState('');
  const [image, setImage] = useState('');


  const clickGlass = (glass) => {
    setName(glass.name)
    setPrice(glass.price)
    setDes(glass.desc)
    setImage(glass.url)
  }


  return (
    <div className="container vglasses py-3">
      <div className="row  justify-content-between">
        <div className="col-6 vglasses__left">
          <div className="row">
            <div className="col-12">
              <h1 className="mb-2">Virtual Glasses</h1>
            </div>
          </div>
          <div className="row" id="vglassesList">
            {
              glassesJson.map(item => <div className="glass-wrap" key={item.id} id={item.id} onClick={() => clickGlass(item)}><img alt="" src={item.url} /></div>)
            }
          </div>
        </div>
        <div className="col-5 vglasses__right p-0">
          <div className="vglasses__card">

            <div className="vglasses__model" id="avatar">
              { name && <img alt="" src={image} />}
            </div>
            <div id="glassesInfo" className="vglasses__info" style={{display: name ? 'block' : 'none'}}>
              <div className="name">{name}</div>
              <div className="price">{price}</div>
              <div className="des">{des}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Glass;
